export const getUserName = (state) => state.userReducer.userName
export const getIsLoggedIn = (state) => state.userReducer.isLoggedIn
export const getDeviceAuthCode = (state) => state.userReducer.deviceAuthCode
export const getCurrentDeviceId = (state) => state.userReducer.currentDeviceId