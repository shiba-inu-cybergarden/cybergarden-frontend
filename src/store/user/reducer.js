import {
    DELETE_DEVICE_AUTH_CODE,
    DELETE_CURRENT_DEVICE_ID,
    SET_CURRENT_DEVICE_ID,
    SET_DEVICE_AUTH_CODE,
    SET_USER_ID,
    SET_USER_LOGGED_IN,
    SET_USER_NAME,
} from './types'

const initState = {
    isLoggedIn: false,
    userName: '',
    userId: '',
    deviceAuthCode: '',
    currentDeviceId: '',
}

export default function userReducer(state = initState, action) {
    switch (action.type) {
        case SET_USER_NAME:
            return {
                ...state,
                userName: action.payload,
            }
        case SET_USER_ID:
            return {
                ...state,
                userId: action.payload,
            }
        case SET_USER_LOGGED_IN:
            return {
                ...state,
                isLoggedIn: true,
            }
        case SET_DEVICE_AUTH_CODE:
            return {
                ...state,
                deviceAuthCode: action.payload,
            }
        case DELETE_DEVICE_AUTH_CODE:
            return {
                ...state,
                deviceAuthCode: '',
            }
        case SET_CURRENT_DEVICE_ID:
            return {
                ...state,
                currentDeviceId: action.payload
            }
        case DELETE_CURRENT_DEVICE_ID:
            return {
                ...state,
                currentDeviceId: ''
            }
        default:
            return state
    }
}
