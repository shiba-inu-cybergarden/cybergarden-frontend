/** Service */
import userAuth from '../../service/userAuth'
import devicesService from '../../service/devicesService'

/** Types */
import {
    SET_DEVICE_AUTH_CODE,
    SET_USER_ID,
    SET_USER_LOGGED_IN,
    SET_USER_NAME,
    DELETE_DEVICE_AUTH_CODE,
    SET_CURRENT_DEVICE_ID,
    DELETE_CURRENT_DEVICE_ID,
} from './types'

export const setUserLoggedIn = () => ({
    type: SET_USER_LOGGED_IN,
    payload: true,
})

export const setUserName = (userName) => ({
    type: SET_USER_NAME,
    payload: userName,
})

export const setUserId = (userId) => ({
    type: SET_USER_ID,
    payload: userId,
})

export const setDeviceAuthCode = (deviceAuthCode) => ({
    type: SET_DEVICE_AUTH_CODE,
    payload: deviceAuthCode,
})

export const deleteDeviceAuthCode = () => ({
    type: DELETE_DEVICE_AUTH_CODE,
})

export const setCurrentDeviceId = (deviceId) => ({
    type: SET_CURRENT_DEVICE_ID,
    payload: deviceId
})

export const deleteCurrentDeviceId = (deviceId) => ({
    type: DELETE_CURRENT_DEVICE_ID,
    payload: deviceId
})

export const userSignUpThunk = (email, password, name) => async (dispatch) => {
    try {
        const response = await userAuth.userSignUp(email, password, name)

        response.status === 'ok' &&
            dispatch(userLoginThunk(email, password))
    } catch (err) {
        console.error(err)
    }
}

export const userLoginThunk = (email, password) => async (dispatch) => {
    try {
        const response = await userAuth.userLogin(email, password)

        if (response.status === 'ok') {
            dispatch(setUserLoggedIn())
            dispatch(setUserName(response.payload.userName))
            dispatch(setUserId(response.payload.userId))
        }
    } catch (err) {
        console.error(err)
    }
}

export const setDeviceAuthCodeThunk = (deviceName) => async (dispatch) => {
    try {
        const response = await devicesService.registerDevice(deviceName)

        if (response.status === 'ok') {
            dispatch(setDeviceAuthCode(response.payload.deviceAuthCode))
        }
    } catch (err) {
        console.error(err)
    }
}
