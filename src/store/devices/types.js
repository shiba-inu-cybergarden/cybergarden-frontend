export const SET_DEVICES_LIST = 'devices/SET_DEVICES_LIST'
export const SET_CHECKED = 'devices/SET_CHECKED'
export const SET_CHECKED_ALL = 'devices/SET_CHECKED_ALL'
export const DELETE_CHECKED_ALL = 'devices/DELETE_CHECKED_ALL'