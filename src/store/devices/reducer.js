import {
    DELETE_CHECKED_ALL,
    SET_CHECKED,
    SET_CHECKED_ALL,
    SET_DEVICES_LIST,
} from './types'

const initState = {
    devices: [
        {
            id: '',
            deviceName: '',
            deviceBrandName: '',
            deviceModel: '',
            deviceOS: '',
            batteryCharge: null,
            coordinates: [],
            height: null,
            isAvailable: true,
            deviceAuthCode: '',
            isAuthComplete: true,
        },
    ],
}

export default function devicesReducer(state = initState, action) {
    switch (action.type) {
        case SET_DEVICES_LIST:
            const devicesArray = [...action.payload]

            devicesArray.forEach((item) => (item.checked = true))

            return {
                ...state,
                devices: [...action.payload],
            }
        case SET_CHECKED:
            return {
                ...state,
                devices: state.devices.map((item) => {
                    if (item.id === action.payload.deviceId) {
                        item.checked = action.payload.checked
                    }

                    return item
                }),
            }
        case SET_CHECKED_ALL:
            return {
                ...state,
                devices: state.devices.map((item) => {
                    item.checked = true
                    return item
                }),
            }
        case DELETE_CHECKED_ALL:
            return {
                ...state,
                devices: state.devices.map((item) => {
                    item.checked = false
                    return item
                }),
            }
        default:
            return state
    }
}
