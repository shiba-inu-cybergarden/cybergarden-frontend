/** Service */
import devicesService from "../../service/devicesService";
import { deleteCurrentDeviceId, setCurrentDeviceId } from "../user/actions";

/** Types */
import { DELETE_CHECKED_ALL, SET_CHECKED, SET_CHECKED_ALL, SET_DEVICES_LIST } from "./types";

export const setDevicesList = (list) => (
    {
        type: SET_DEVICES_LIST,
        payload: list
    }
)

export const setDeviceChecked = (checked, deviceId) => (
    {
        type: SET_CHECKED,
        payload: {
            checked,
            deviceId
        }
    }
)

export const setCheckedAllDevices = () => ({
    type: SET_CHECKED_ALL
})

export const deleteCheckedAllDevices = () => ({
    type: DELETE_CHECKED_ALL
})

export const getDevicesThunk = () => async (dispatch) => {
    try {
        const response = await devicesService.getDevicesRequest()

        response.status === 'ok' && dispatch(setDevicesList(response.payload.devicesData))
    } catch (err) {
        console.error(err)
    }
}

export const takeDeviceThunk = (deviceId) => async (dispatch, getState) => {
    try {
        const userId = getState().userReducer.userId

        const response = await devicesService.takeDevice(deviceId, userId)

        if (response.status === 'ok') { 
            dispatch(setCurrentDeviceId(deviceId))
            dispatch(getDevicesThunk())
        }
    } catch (err) {
        console.error(err)
    }
}

export const returnDeviceThunk = (deviceId) => async (dispatch, getState) => {
    try {
        const userId = getState().userReducer.userId

        const response = await devicesService.returnDevice(deviceId, userId)

        if (response.status === 'ok') { 
            dispatch(deleteCurrentDeviceId())
            dispatch(getDevicesThunk())
        }
    } catch (err) {
        console.error(err)
    }
}