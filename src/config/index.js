export const routes = {
    home: '/',
    dashboard: '/dashboard',
    map: '/map',
    devices: '/devices',
    registration: '/registration',
}

export const defaultMapCenter = [38.935305, 47.202324]

export const baseUrl = 'https://find2test.ru/api'