/** Material ui components */
import AppBar from '@mui/material/AppBar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'

/** Hooks */
import { useLocation } from 'react-router-dom'
import { useCallback, useState } from 'react'

/** Styles */
import styles from './index.module.scss'

/** Config */
import { routes } from '../../config'

/** Components */
import SideMenu from '../../components/SideMenu'
import AddDeviceModalWindow from '../../components/AddDeviceModalWindow'

/** Redux */
import { useSelector } from 'react-redux'
import { getUserName } from '../../store/user/selectors'

export default function PageLayout({ children }) {
    const [isOpen, setIsOpen] = useState(false)

    const location = useLocation()
    const userName = useSelector((state) => getUserName(state))

    const getHeaderTitle = useCallback(() => {
        switch (location.pathname) {
            case routes.dashboard:
                return 'Дашборд'
            case routes.devices:
                return 'Список устройств'
            case routes.map:
                return 'Карта'
        }
    }, [location])

    return (
        <div>
            <SideMenu userName={userName} />
            <div className={styles.pageWrapper}>
                <AppBar position="static" color="inherit">
                    <div className={styles.pageHeader}>
                        <Typography variant="h6" component="div">
                            {getHeaderTitle()}
                        </Typography>
                        {location.pathname === routes.dashboard && (
                            <Button variant="contained" onClick={() => setIsOpen(true)}>
                                Добавить устройство
                            </Button>
                        )}
                    </div>
                </AppBar>
                {children}
                <AddDeviceModalWindow isOpen={isOpen} setIsOpen={setIsOpen} />
            </div>
        </div>
    )
}
