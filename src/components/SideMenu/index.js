/** React, hooks */
import { useNavigate } from 'react-router'

/** Material components */
import Drawer from '@mui/material/Drawer'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Avatar from '@mui/material/Avatar'
import Divider from '@mui/material/Divider'

/** Material icons */
import LocationOnIcon from '@mui/icons-material/LocationOn'
import DashboardIcon from '@mui/icons-material/Dashboard'
import ListAltIcon from '@mui/icons-material/ListAlt'

/** Styles */
import styles from './index.module.scss'

/** Config */
import { routes } from '../../config'

export default function SideMenu({userName}) {
    let navigate = useNavigate()

    const menuItems = [
        {
            name: 'Дашборд',
            icon: <DashboardIcon />,
            route: routes.dashboard,
        },
        {
            name: 'Список устройств',
            icon: <ListAltIcon />,
            route: routes.devices,
        },
    ]

    return (
        <Drawer variant="permanent" open>
            <List>
                <div className={styles.userWrapper}>
                    <Avatar alt="Avatar" />
                    <span className={styles.userName}>{userName}</span>
                </div>
                <Divider />
                {menuItems.map((item) => {
                    return (
                        <ListItem
                            button
                            sx={{ width: '264px' }}
                            key={item.name}
                            onClick={() => navigate(item.route)}
                        >
                            <ListItemIcon
                                sx={{
                                    minWidth: '24px',
                                    marginRight: '16px',
                                }}
                            >
                                {item.icon}
                            </ListItemIcon>
                            <ListItemText primary={item.name} />
                        </ListItem>
                    )
                })}
            </List>
        </Drawer>
    )
}
