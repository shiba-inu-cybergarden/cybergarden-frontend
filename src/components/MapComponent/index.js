/** React */
import { useEffect, useState } from 'react'

/** MapBox */
import ReactMapboxGl, { Marker, Popup } from 'react-mapbox-gl'
import 'mapbox-gl/dist/mapbox-gl.css'

/** Config */
import { mapBoxToken } from '../../config/tokens'
import { defaultMapCenter } from '../../config'

/** Material ui */
import MarkerIcon from '@mui/icons-material/Room'
import UserMarkerIcon from '@mui/icons-material/PersonPinCircle'
import Button from '@mui/material/Button'

/** Styles */
import styles from './index.module.scss'

/** Components */
import DeviceDetailsModalWindow from '../DeviceDetailsModalWindow'

const Map = ReactMapboxGl({
    accessToken: mapBoxToken,
    minZoom: 17,
    maxZoom: 20,
})

export default function MapComponent(props) {
    const { width, height, devices } = props

    const [userLocation, setUserLocation] = useState([])
    const [popup, setPopup] = useState({
        show: false,
        coordinates: [],
        name: '',
        lastUser: '',
    })
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [currentDevice, setCurrentDevice] = useState(null)

    useEffect(() => {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                // setUserLocation([
                //     position.coords.longitude,
                //     position.coords.latitude,
                // ])
            })
            setUserLocation(defaultMapCenter)
        } else {
            setUserLocation(defaultMapCenter)
        }
    }, [])

    const onMarkerClick = (coordinates, deviceGeneralName, details) => () => {
        setPopup({
            show: !popup.show,
            coordinates,
            name: deviceGeneralName,
            details,
        })
    }

    const handleOpenDetails = (value) => () => {
        setCurrentDevice(value)
        setIsModalOpen(true)
    }

    return (
        <div className={styles.mapWrapper} style={{ width, height }}>
            {userLocation.length > 0 && (
                <Map
                    style="mapbox://styles/mapbox/streets-v9"
                    containerStyle={{
                        height: height,
                        width: width,
                    }}
                    zoom={[17]}
                    center={userLocation}
                    onClick={() => {
                        setPopup({
                            show: false,
                            coordinates: [],
                        })
                    }}
                >
                    <Marker coordinates={userLocation} anchor="bottom">
                        <UserMarkerIcon
                            color="info"
                            fontSize="large"
                            sx={{ color: 'green', fontSize: 50 }}
                        />
                    </Marker>
                    {devices.map((item) => {
                        return (
                            item.checked &&
                            item?.isAuthComplete && (
                                <Marker
                                    coordinates={item.coordinates}
                                    anchor="bottom"
                                    key={item.id}
                                >
                                    <MarkerIcon
                                        color={
                                            item.isAvailable ? 'info' : 'error'
                                        }
                                        fontSize="large"
                                        onClick={onMarkerClick(
                                            item.coordinates,
                                            `${item.deviceBrandName} ${item.deviceModel}`,
                                            item
                                        )}
                                        style={{ cursor: 'pointer' }}
                                    />
                                </Marker>
                            )
                        )
                    })}
                    {popup.show && (
                        <Popup
                            coordinates={popup.coordinates}
                            offset={{
                                'bottom-left': [12, -38],
                                bottom: [0, -38],
                                'bottom-right': [-12, -38],
                            }}
                        >
                            <div className={styles.popup}>
                                <span className={styles.popupDeviceName}>
                                    {popup.name}
                                </span>
                                <Button
                                    onClick={handleOpenDetails(popup.details)}
                                >
                                    Подробнее
                                </Button>
                            </div>
                        </Popup>
                    )}
                </Map>
            )}
            {isModalOpen && (
                <DeviceDetailsModalWindow
                    isOpen={isModalOpen}
                    setIsOpen={setIsModalOpen}
                    device={currentDevice}
                />
            )}
        </div>
    )
}
