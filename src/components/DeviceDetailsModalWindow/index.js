/** Material ui components */
import Modal from '@mui/material/Modal'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'

/** Styles */
import styles from './index.module.scss'

/** Redux */
import { useDispatch, useSelector } from 'react-redux'
import { returnDeviceThunk, takeDeviceThunk } from '../../store/devices/actions'
import { getCurrentDeviceId } from '../../store/user/selectors'
import { useEffect, useState } from 'react'
import devicesService from '../../service/devicesService'

export default function DeviceDetailsModalWindow(props) {
    const { isOpen, setIsOpen, device } = props
    const [floor, setFloor] = useState()

    useEffect(() => {
        (async () => {
            if (device) {
                const response = await devicesService.getDeviceFloor(device.coordinates)
                if (response.status === 'ok') {
                    setFloor(response.payload?.floor)
                }
            }
        })()
    }, [device])

    const dispatch = useDispatch()
    const currentDeviceId = useSelector((state) => getCurrentDeviceId(state))

    const handleClose = () => {
        setIsOpen(false)
    }

    const onTakeDevice = (deviceId) => () => {
        dispatch(takeDeviceThunk(deviceId))
        setIsOpen(false)
    }

    const onReturnDevice = (deviceId) => () => {
        dispatch(returnDeviceThunk(deviceId))
        setIsOpen(false)
    }

    return (
        <Modal open={isOpen} onClose={handleClose}>
            <div className={styles.modalWindow}>
                <Typography
                    variant="h6"
                    component="h2"
                    sx={{ marginBottom: '16px' }}
                >
                    {device.deviceName}
                </Typography>
                <span className={styles.description}>
                    Модель устройства:{' '}
                    <b>{`${device.deviceBrandName} ${device.deviceModel}`}</b>
                </span>
                <span className={styles.description}>
                    Операционная система: <b>{device.deviceOS}</b>
                </span>
                <span className={styles.description}>
                    Последний пользователь:{' '}
                    <b>{device.userNameTakenDevice || 'нет'}</b>
                </span>
                {floor && <span className={styles.description}>
                    Этаж: <b>{floor}</b>
                </span>}
                <span className={styles.description}>
                    Уровень заряда: <b>{device.batteryCharge + '%'}</b>
                </span>
                <span className={styles.description}>
                    Статус: <b>{device.isAvailable ? 'свободно' : 'занято'}</b>
                </span>
                {currentDeviceId !== device.id ? (
                    <Button
                        variant="contained"
                        disabled={!device.isAvailable}
                        onClick={onTakeDevice(device.id)}
                    >
                        Занять
                    </Button>
                ) : (
                    <Button
                        variant="contained"
                        onClick={onReturnDevice(device.id)}
                    >
                        Вернуть
                    </Button>
                )}
            </div>
        </Modal>
    )
}
