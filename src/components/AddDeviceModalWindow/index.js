/** React */
import { useEffect, useState } from 'react'

/** Material ui components */
import Modal from '@mui/material/Modal'
import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'

/** Styles */
import styles from './index.module.scss'

/** Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getDeviceAuthCode } from '../../store/user/selectors'
import { deleteDeviceAuthCode, setDeviceAuthCode, setDeviceAuthCodeThunk } from '../../store/user/actions'

export default function AddDeviceModalWindow(props) {
    const { isOpen, setIsOpen } = props

    const deviceAuthCode = useSelector((state) => getDeviceAuthCode(state))
    const dispatch = useDispatch()

    const [deviceName, setDeviceName] = useState('')
    const [isFormValid, setIsFormValid] = useState(false)

    useEffect(() => {
        dispatch(deleteDeviceAuthCode())
    }, [])

    useEffect(() => {
        if (deviceName.length > 4) {
            setIsFormValid(true)
        } else {
            setIsFormValid(false)
        }
    }, [deviceName])

    const handleClose = () => {
        setIsOpen(false)
        setIsFormValid(false)
        setDeviceName(false)
        dispatch(deleteDeviceAuthCode())
    }

    const onFormSubmit = () => {
        dispatch(setDeviceAuthCodeThunk(deviceName))
    }

    return (
        <Modal open={isOpen} onClose={handleClose}>
            <div className={deviceAuthCode ? styles.modalWindowAuthCode : styles.modalWindow}>
                <Typography
                    variant="h6"
                    component="h2"
                    sx={{ marginBottom: '16px' }}
                >
                    Добавление устройства
                </Typography>
                {!deviceAuthCode && (
                    <>
                        <span className={styles.description}>
                            Для добавления гаджета в сервис, вам нужно получить
                            код для регистрации устройства, и ввести его в
                            мобильном приложении Find2Test
                        </span>
                        <TextField
                            label="Название устройства"
                            variant="outlined"
                            onChange={(e) => setDeviceName(e.target.value)}
                            sx={{ marginBottom: '16px', width: '200px' }}
                        />
                        <Button
                            variant="contained"
                            disabled={!isFormValid}
                            onClick={onFormSubmit}
                        >
                            Получить код
                        </Button>
                    </>
                )}
                {deviceAuthCode && <>
                    <span className={styles.deviceCodeDescription}>
                        Код для авторизации устройства:
                    </span>
                    <b>
                        {deviceAuthCode}
                    </b>
                </>}
            </div>
        </Modal>
    )
}
