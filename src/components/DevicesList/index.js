/** React */
import { useState } from 'react'

/** Material ui components */
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Checkbox from '@mui/material/Checkbox'
import ListSubheader from '@mui/material/ListSubheader'
import AvailableIcon from '@mui/icons-material/CheckCircle'
import BusyIcon from '@mui/icons-material/DoDisturbOn'
import IconButton from '@mui/material/IconButton'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import Divider from '@mui/material/Divider'

/** Styles */
import styles from './index.module.scss'

/** Redux */
import { useDispatch } from 'react-redux'
import {
    deleteCheckedAllDevices,
    setCheckedAllDevices,
    setDeviceChecked,
} from '../../store/devices/actions'

/** Components */
import DeviceDetailsModalWindow from '../DeviceDetailsModalWindow'

export default function DevicesList(props) {
    const { width, height, devices } = props

    const [isModalOpen, setIsModalOpen] = useState(false)
    const [currentDevice, setCurrentDevice] = useState(null)
    const [checkedAll, setCheckedAll] = useState(true)

    const dispatch = useDispatch()

    const handleChecked = (value) => () => {
        dispatch(setDeviceChecked(!value.checked, value.id))
    }

    const handleCheckedAll = () => {
        if (checkedAll) {
            dispatch(deleteCheckedAllDevices())
            setCheckedAll(false)
        } else {
            dispatch(setCheckedAllDevices())
            setCheckedAll(true)
        }
    }

    const handleOpenDetails = (value) => () => {
        setCurrentDevice(value)
        setIsModalOpen(true)
    }

    return (
        <div className={styles.devicesListWrapper} style={{ width, height }}>
            <List
                disablePadding
                subheader={
                    <ListSubheader>
                        <Checkbox
                            edge="start"
                            checked={checkedAll}
                            disableRipple
                            onClick={handleCheckedAll}
                        />
                        <span className={styles.devicesListHeader}>
                            Устройства
                        </span>
                        <Divider />
                    </ListSubheader>
                }
            >
                {devices.map((value) => {
                    const labelId = `checkbox-list-label-${value.id}`

                    return (
                        value?.isAuthComplete && (
                            <ListItem
                                key={value.id}
                                disablePadding
                                secondaryAction={
                                    <span className={styles.deviceButtons}>
                                        {value.isAvailable ? (
                                            <AvailableIcon color="success" />
                                        ) : (
                                            <BusyIcon color="error" />
                                        )}
                                        <IconButton
                                            onClick={handleOpenDetails(value)}
                                        >
                                            <MoreVertIcon />
                                        </IconButton>
                                    </span>
                                }
                            >
                                <ListItemButton
                                    role={undefined}
                                    dense
                                    onClick={handleChecked(value)}
                                >
                                    <ListItemIcon>
                                        <Checkbox
                                            edge="start"
                                            checked={value.checked}
                                            disableRipple
                                            inputProps={{
                                                'aria-labelledby': labelId,
                                            }}
                                        />
                                    </ListItemIcon>
                                    <ListItemText
                                        id={labelId}
                                        primary={`${value.deviceName}: ${value.deviceBrandName} ${value.deviceModel} ${value.deviceOS}`}
                                        sx={{ maxWidth: '200px' }}
                                    />
                                </ListItemButton>
                            </ListItem>
                        )
                    )
                })}
            </List>
            {isModalOpen && (
                <DeviceDetailsModalWindow
                    isOpen={isModalOpen}
                    setIsOpen={setIsModalOpen}
                    device={currentDevice}
                />
            )}
        </div>
    )
}
