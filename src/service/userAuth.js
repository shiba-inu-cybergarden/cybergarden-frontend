/** Config */
import { baseUrl } from '../config'

class UserAuth {
    /**
     * User sign up
     * @param {string} email
     * @param {string} password
     * @param {string} name
     * @returns
     */
    async userSignUp(email, password, name) {
        const url = `${baseUrl}/signUp`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
                name,
            }),
        })

        return await response.json()
    }

    async userLogin(email, password) {
        const url = `${baseUrl}/login`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
            }),
        })

        return await response.json()
    }
}

export default new UserAuth()
