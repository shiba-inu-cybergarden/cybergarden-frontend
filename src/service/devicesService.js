/** Config */
import { baseUrl } from '../config'

class DevicesService {
    async getDevicesRequest() {
        const url = `${baseUrl}/getAllDevices`

        const response = await fetch(url, {
            method: 'get',
        })

        return await response.json()
    }

    async registerDevice(deviceName) {
        const url = `${baseUrl}/createDevice`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                deviceName,
            }),
        })

        return await response.json()
    }

    async takeDevice(deviceId, userId) {
        const url = `${baseUrl}/takeDevice`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                deviceId,
                userId,
            }),
        })

        return await response.json()
    }

    async returnDevice(deviceId, userId) {
        const url = `${baseUrl}/putDeviceBack`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                deviceId,
                userId,
            }),
        })

        return await response.json()
    }

    async getDeviceFloor(coordinates) {
        const url = `${baseUrl}/getZoneInfoByCoordinates`

        const response = await fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                coordinates,
            }),
        })

        return await response.json()
    }
}

export default new DevicesService()
