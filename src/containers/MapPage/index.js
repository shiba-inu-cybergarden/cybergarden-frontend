/** Components */
import MapComponent from '../../components/MapComponent'

/** Redux */
import { useSelector } from 'react-redux'
import { getDevices } from '../../store/devices/selectors'

/** Styles */
import styles from './index.module.scss'

export default function MapPage() {
    const devices = useSelector((state) => getDevices(state))

    return (
        <div>
            <MapComponent width="82.5vw" height="91vh" devices={devices} />
        </div>
    )
}
