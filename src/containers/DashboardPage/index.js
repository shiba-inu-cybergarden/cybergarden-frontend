/** React */
import { useEffect } from 'react'

/** Components */
import DevicesList from '../../components/DevicesList'
import MapComponent from '../../components/MapComponent'

/** Styles */
import styles from './index.module.scss'

/** Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getDevices } from '../../store/devices/selectors'
import { getDevicesThunk } from '../../store/devices/actions'

export default function DashboardPage() {
    const dispatch = useDispatch()
    const devices = useSelector(state => getDevices(state))

    useEffect(() => {
        dispatch(getDevicesThunk())
    }, [])

    return (
        <div className={styles.pageWrapper}>
            <MapComponent width='800px' height='600px' devices={devices} />
            <DevicesList width='400px' height='600px' devices={devices} />
        </div>
    )
}