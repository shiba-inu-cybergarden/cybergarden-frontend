/** React */
import { useEffect, useRef, useState } from 'react'

/** Material ui components */
import Button from '@mui/material/Button'
import { DataGrid } from '@mui/x-data-grid'

/** Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getDevices } from '../../store/devices/selectors'
import { getDevicesThunk } from '../../store/devices/actions'

/** Styles */
import styles from './index.module.scss'

export default function DevicesListPage() {
    const [rows, setRows] = useState([])
    const dispatch = useDispatch()
    const devices = useSelector(state => getDevices(state))

    useEffect(() => {
        dispatch(getDevicesThunk())
    }, [])

    useEffect(() => {
        if (devices && devices.length) {
            const rowsArray = makeRows()
            setRows(rowsArray)
        }
    }, [devices])

    const tableColumns = useRef([
        { field: 'status', headerName: 'Статус', width: 120 },
        { field: 'deviceModel', headerName: 'Устройство', width: 130 },
        { field: 'battery', headerName: 'Уровень заряда', width: 160 },
        { field: 'os', headerName: 'ОС', width: 140 },
        { field: 'zone', headerName: 'Отдел', width: 130 },
        { field: 'lastUser', headerName: 'Последний пользователь', width: 200 },
    ])

    const makeRows = () => {
        const rows = []

        devices.forEach((item) => {
            rows.push({
                id: item.id,
                status: item.isAvailable ? 'Свободно' : 'Занято',
                deviceModel: item.deviceBrandName + ' ' + item.deviceModel,
                battery: item.batteryCharge + '%',
                os: item.deviceOS,
                zone: 'Отдел тестирования',
                lastUser: 'Нет',
            })
        })

        return rows
    }

    return (
        <div className={styles.pageWrapper}>
            {/* <Button variant="contained">
                Зарегистрировать устройство
            </Button> */}
            <div className={styles.tableWrapper}>
                <DataGrid
                    rows={rows}
                    columns={tableColumns.current}
                    pageSize={5}
                />
            </div>
        </div>
    )
}
