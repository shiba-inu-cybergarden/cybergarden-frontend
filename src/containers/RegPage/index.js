/** React */
import { useEffect, useState } from 'react'

/** Material ui components */
import TextField from '@mui/material/TextField'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Link from '@mui/material/Link'

/** Styles */
import styles from './index.module.scss'

/** Redux */
import { useDispatch } from 'react-redux'
import { userLoginThunk, userSignUpThunk } from '../../store/user/actions'

/** Router */
import { useNavigate } from 'react-router-dom'

/** Config */
import { routes } from '../../config'

export default function RegPage({ isLoggedIn }) {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isFormValid, setIsFormValid] = useState(false)
    const [isLoginForm, setIsLoginForm] = useState(false)

    const navigate = useNavigate()
    const dispatch = useDispatch()

    useEffect(() => {
        if (
            !isLoginForm
                ? name.length > 1 && email.length > 5 && password.length > 6
                : email.length > 5 && password.length
        ) {
            setIsFormValid(true)
        } else {
            setIsFormValid(false)
        }
    }, [name, email, password])

    useEffect(() => {
        isLoggedIn && navigate(routes.dashboard)
    }, [isLoggedIn])

    const onFormSubmit = () => {
        if (!isLoginForm) {
            dispatch(userSignUpThunk(email, password, name))
        } else {
            dispatch(userLoginThunk(email, password, name))
        }
    }

    return (
        <div className={styles.regPageWrapper}>
            <Paper elevation={3} sx={{ width: 400 }}>
                <div className={styles.regPageFormWindow}>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ marginBottom: '16px' }}
                    >
                        {isLoginForm ? 'Авторизация' : 'Регистрация'}
                    </Typography>
                    {!isLoginForm && (
                        <TextField
                            label="Имя"
                            variant="outlined"
                            sx={{ marginBottom: '16px' }}
                            onChange={(e) => setName(e.target.value)}
                        />
                    )}
                    <TextField
                        label="Email"
                        variant="outlined"
                        sx={{ marginBottom: '16px' }}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <TextField
                        label="Пароль"
                        variant="outlined"
                        type="password"
                        sx={{ marginBottom: '16px' }}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <Button
                        variant="contained"
                        sx={{ marginBottom: '16px' }}
                        disabled={!isFormValid}
                        onClick={onFormSubmit}
                    >
                        {isLoginForm ? 'Войти' : 'Подтвердить'}
                    </Button>
                    {!isLoginForm && (
                        <Link
                            sx={{ marginBottom: '16px', cursor: 'pointer' }}
                            onClick={() => setIsLoginForm(true)}
                        >
                            Уже есть аккаунт
                        </Link>
                    )}
                </div>
            </Paper>
        </div>
    )
}
