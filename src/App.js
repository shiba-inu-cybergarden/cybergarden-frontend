/** Router */
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom'

/** Layouts */
import PageLayout from './layouts/PageLayout'

/** Containers */
import MapPage from './containers/MapPage'
import DashboardPage from './containers/DashboardPage'
import DevicesListPage from './containers/DevicesListPage'
import RegPage from './containers/RegPage'

/** Config */
import { routes } from './config'

/** Redux */
import { useSelector } from 'react-redux'
import { getIsLoggedIn } from './store/user/selectors'

export default function App() {
    const isLoggedIn = useSelector(state => getIsLoggedIn(state))

    return (
        <BrowserRouter>
            <Routes>
                <Route
                    exact
                    path={routes.home}
                    element={<Navigate to={isLoggedIn ? routes.dashboard : routes.registration} />}
                />
                <Route
                    exact
                    path={routes.dashboard}
                    element={<PageLayout children={<DashboardPage />} />}
                />
                <Route 
                    exact 
                    path={routes.map} 
                    element={<PageLayout children={<MapPage />} />} 
                />
                <Route
                    exact
                    path={routes.devices}
                    element={<PageLayout children={<DevicesListPage />} />}
                />
                <Route
                    exact
                    path={routes.registration}
                    element={<RegPage isLoggedIn={isLoggedIn} />}
                />
            </Routes>
        </BrowserRouter>
    )
}
